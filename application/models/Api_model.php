<?php

Class Api_model extends CI_Model
{
	
	public function __construct()
	{
		$this->load->database();
	}
    
    /*
    This function will check mashroom against its rules from its rules table 'mashroom_rules'
    */
	public function check_mashroom($rvals){
       
        $length = $rvals['length'];
        $weight = $rvals['weight'];
        $color = $rvals['color'];
        
        $this->db->select('*');
        $this->db->where("$length BETWEEN length_from AND length_to");
        $this->db->where("$weight BETWEEN weight_from AND weight_to");
        $this->db->where('color',$color);
               
		$query = $this->db->get('mashroom_rules');
      
        return $query->num_rows();
	}	
		
}
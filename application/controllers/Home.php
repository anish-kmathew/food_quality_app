<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * This the home page where user can check the foods
	 */
	public function index()
	{
		$this->load->view('home');
	}
}

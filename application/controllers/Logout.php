<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * This is used to logout
	 */
	public function index()
	{
        //unset the cookie where we store token
        setcookie("food_check_token", "", 0, "/", NULL, NULL, true );
        redirect('/Login', 'refresh');		
	}
}

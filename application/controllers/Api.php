<?php
/**
 * This is  implementation of a RESTful API. The database contains a
 * `users` table with four columns (`user_id`, `username`, `password` and
 * `email`). And the rules table for mashroom, we have to create one table each for each food
 * 
 */
class Api extends CI_Controller {

  private $users, $auth;

  public function __construct(){
    parent::__construct();

    //Loading the api model for rules checking
    $this->load->model('api_model');

    $this->load->helper('error_code');
    $this->load->helper('database');
    $this->load->helper('jwt');
    $this->load->helper('request');
    $this->load->helper('auth');
    $this->load->helper('rest_api');
    $this->users = array(
      "table" => "`users`",
      "create_fields" => ["`username`", "`password`", "`email`"],
      "create_types" => "sss",
      "read_fields" => "`username`, `user_id` AS `id`",
      "read_key" => "`username`",
      "update_fields" => ["`email`"],
      "update_key" => "`username`",
      "delete_key" => "`username`"
    );
    $this->auth = array(
      "table" => "`users`",
      "fields" => "`username`, `user_id` AS `id`, `password`",
      "username_field" => "`username`",
      "password_field" => "`password`",
      "id_field" => "`id`",
      "service_name" => "food_check",
      "cookie_name" => "food_check_token"
    );
    $this->food = array(
      "table" => "`api`",
      "read_fields" => "`service_name`, `id`,`attributes`",
      "read_key" => "`service_name`"
    );
  }

  public function login(){
    header('Content-Type: application/json');
   
    if($this->input->method(true) != 'POST'){
        echo json_encode(array(
          "code" => BAD_METHOD,
          "alert"=> 'alert-danger',
          "message"=>"Use the HTTP POST method to login to the system."
        ));
      return;
    }
    else
      if(check_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"])){
        echo json_encode(regenerate_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"]));
        return;
      }
      else {
        echo json_encode(authorize($this->auth["table"], $this->auth["fields"],
          $this->auth["username_field"], $this->auth["password_field"], $this->auth["id_field"],
          $_POST["username"], hash("sha512", $_POST["password"], true),
          $this->auth["service_name"], $this->auth["cookie_name"]
        ));
        return;
      }
  }


  public function register($param=''){

      $username = $_POST["username"];
      $password = $_POST['password'];
      $email = $_POST['email'];

      //validating the registration 
      $is_valid = true;
      $err_msg  = ' ';
      if (trim($username)==''){
        $is_valid = false;
        $err_msg  .= " You should select a username.";
      }else{
        $data = readResourceElement(
          $this->users["table"], $this->users["read_fields"], $this->users["read_key"],
          $username
        );
        if($data){
          $is_valid = false;
           $err_msg  .= " Username already exists.";
        }       
      }
      if (trim($email)==''){
        $is_valid = false;
        $err_msg  .= " You should select an email.";
      }else{
        $data = readResourceElement(
          $this->users["table"], $this->users["read_fields"], 'email',
          $email
        );
        if($data){
          $is_valid = false;
           $err_msg  .= " Email already exists.";
        }       
      }
      if (trim($password)==''){
        $is_valid = false;
        $err_msg  .= " Password cannot be empty.";
      }
      if (!$is_valid){
        echo json_encode(array(
          "code" => BAD_DATA,
          "alert"=> 'alert-danger',
          "message" => $err_msg
        ));
        return;
      }

      //Confirming the post method and registering the user
      if($this->input->method(true) == 'POST'){
        createResourceElement(
        $this->users["table"], $this->users["create_fields"], $this->users["create_types"],
        [$_POST["username"],
        password_hash(hash("sha512", $_POST["password"], true), PASSWORD_DEFAULT),
        $_POST["email"]]
      );
      echo json_encode(array(
        "code" => SUCCESS,
        "alert"=> 'alert-success',
        "message" => "Registration Successful! Login now"
      ));
      return;
    }
  }


  /**
   * Method for accessing `user` resources.
   * Parameters:
   * @param $param - (Optional) The parameter used to uniquely identify the
   *  specific resource. If nothing is specified, the root resource will be
   *  set as the target.
   */
  public function users($param=''){
    header('Content-Type: application/json');
    // Create - POST
    if($this->input->method(true) == 'POST'){
        echo json_encode(createResourceElement(
        $this->users["table"], $this->users["create_fields"], $this->users["create_types"],
        [$_POST["username"],
        password_hash(hash("sha512", $_POST["password"], true), PASSWORD_DEFAULT),
        $_POST["email"]]
      ));
      return;
    }
    // Update - PUT
    if($this->input->method(true) == 'PUT'){
      if(check_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"])){
        $jwt = get_jwt_data($this->auth["cookie_name"]);
        $_PUT = get_request_body();
        if(isset($jwt["username"]) && $jwt["username"] == $_PUT["username"]) {
          echo json_encode(updateResourceElement(
            $this->users["table"], $this->users["update_fields"], "s",
            [$_PUT["email"]], $this->users["update_key"], $_PUT["username"]
          ));
          return;
        }
        echo json_encode(array(
          "code" => BAD_CREDENTIALS,
          "alert"=> 'alert-danger',
          "message" => "Token does not match the provided credentials."
        ));
        return;
      }
      echo json_encode(array(
        "code" => NO_COOKIE,
        "alert"=> 'alert-danger',
        "message" => "Token not found or invalid."
      ));
      return;
    }
    // Delete - DELETE
    if($this->input->method(true) == 'DELETE'){
      if(check_jwt_cookie($this->auth["service_name"], $this->auth["cookie_name"])){
        $jwt = get_jwt_data($this->auth["cookie_name"]);
        $_DELETE = get_request_body();
        if(isset($jwt["username"]) && $jwt["username"] == $_DELETE["username"]) {
          echo json_encode(deleteResourceElement(
            $this->users["table"], $this->users["delete_key"], $_DELETE["username"]
          ));
          return;
        }
        echo json_encode(array(
          "code" => BAD_CREDENTIALS,
          "alert"=> 'alert-danger',
          "message" => "Token does not match the provided credentials."
        ));
        return;
      }
      echo json_encode(array(
        "code" => NO_COOKIE,
        "alert"=> 'alert-danger',
        "message" => "Token not found or invalid."
      ));
      return;
    }
    // Read - GET
    if($param == ''){
      echo json_encode(readResourceRoot( $this->users["table"], $this->users["read_fields"]));
      return;
    }
    else{
      $data = readResourceElement(
        $this->users["table"], $this->users["read_fields"], $this->users["read_key"],
        $param
      );
      if($data == false)
        echo json_encode(array(
          "code" => BAD_DATA,
          "alert"=> 'alert-danger',
          "message" => "No user with this username."
        ));
      else
        echo json_encode($data[0]);
    }
  }

  /**
   * Index method for completeness. Returns a JSON response with an
   * error message.
   */
  public function index(){
    header('Content-Type: application/json');
    echo json_encode(array(
      "code" => BAD_DATA,
      "alert"=> 'alert-danger',
      "message"=>"No resource specified."
  ));
  }



  /* Food Quality related endpoints */


  /**
   * Method for accessing `food` resources.
   * Parameters:
   * @param $param - (Optional) The parameter used to uniquely identify the
   *  specific resource. If nothing is specified, the root resource will be
   *  set as the target.
   */
  public function food($version='',$service=''){
    header('Content-Type: application/json');    
    //fetching the user defined header, here its the token
    $jwt_token =  $_SERVER['HTTP_X_HEADER_JWT'];

    //Authentication
       
    if(!check_jwt_token($this->auth["service_name"], $jwt_token)){
      echo json_encode(array(
        "code" => INVALID_TOKEN,
        "alert"=> 'alert-danger',
        "message" => "Token not found or invalid."
      ));
      return;
    }
    if($this->input->method(true) != 'GET'){                        
      echo json_encode(array(
        "code" => BAD_METHOD,
        "alert"=> 'alert-danger',
        "message" => "Use the HTTP GET method to check the food."
      ));
      return;
    }
  
    //First version of this food quality check Api endpoint
    if ($service="poisonous" && $version = 'v1'){ 
     
      $food = $_GET['food'];
      $is_poisonous = false;

      if (trim($food)==''){
        $err_msg  .= "You should select a food.";
        echo json_encode(array(
          "code" => BAD_DATA,
          "alert"=> 'alert-danger',
          "message" => $err_msg
        ));
        return;
      }
     
      if ($food == 'mashroom'){
          //This will check for the food mashroom. Each food may have its on different rules
          $length = $_GET['length'];
          $weight = $_GET['weight'];
          $color = $_GET['color'];

          $vals = array('length'=>$length,'weight'=>$weight,'color'=>$color);
          

          //validation
          $is_valid = true;
          $err_msg  = ' ';
         
          if (trim($length)=='' || !is_numeric(trim($length))){
            $is_valid = false;
            $err_msg  .= " Length is invalid.";
          }
          if (trim($weight)=='' || !is_numeric(trim($weight))){
            $is_valid = false;
            $err_msg  .= " Weight is invalid.";
          }
          if (trim($color)==''){
            $is_valid = false;
            $err_msg  .= " Color cannot be empty.";
          }
          if (!$is_valid){
            echo json_encode(array(
              "code" => BAD_DATA,
              "alert"=> 'alert-danger',
              "message" => $err_msg
            ));
            return;
          }
        $is_poisonous 	= 	$this->api_model->check_mashroom($vals);
    }
    if ($is_poisonous){
      echo json_encode(array(
        "code" => SUCCESS,
        "alert"=> 'alert-danger',
        "message" => "Dont eat, its poisonous!"
      ));
    }else{
      echo json_encode(array(
        "code" => SUCCESS,
        "alert"=> 'alert-success',
        "message" => "Eat up its safe!"
      ));
    }                    

    }
      
  }

}
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once('header.php');
?>

  <div class="register-box-body">
  <div class="col-xs-12">
        <div class="col-xs-8"></div>
            <div class="col-xs-4">
            <form action="<?php echo base_url(); ?>index.php/Logout" method="POST">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Logout</button>
            </form>
            </div>
        </div>
    <p class="login-box-msg">Check Poisonous</p>
    <div class="row">
        
       
        <!-- /.col -->
      </div>
    <form action="" method="post" id='fq'>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Food Item</label>
        <select class="form-control" id="exampleFormControlSelect1" name="food">
        <option value="mashroom" selected>Mashroom</option>
        </select>
    </div> 
      
    <div class="row">
    <div class="form-group col-md-4">
        <div>Length(cm)</div> 
    </div>
    <div class="form-group col-md-4">
        <input type="text" class="form-control"   placeholder="Length" name="length">
    </div>
 </div>
 <div class="row">
    <div class="form-group col-md-4">
        <div>Weight(gm)</div> 
    </div>
    <div class="form-group col-md-4">
        <input type="text" class="form-control"   placeholder="Weight" name="weight">
    </div>
</div>
  <div class="row">

    <div class="form-group col-md-4">
        <div>Color</div> 
    </div>
    <div class="form-group col-md-4">
        <input type="text" class="form-control"   placeholder="Color" name="color">
    </div>
    </div>

    <div style="display:none" class="alert " role="alert" id='msg'>
    
    </div>

      <div class="row">
        
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Check Quality</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.form-box -->
</div><!-- jQuery-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Bootstrap JS-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script>
     
  // magic.js
$(document).ready(function() {

<?php if (isset($_COOKIE['food_check_token'])){?>
    var jwt_token  =   '<?php echo $_COOKIE['food_check_token']?>';
        //if not logged in redirect to login page
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '<?php echo base_url(); ?>index.php/api/login', // the url where we want to POST
            data        : {}, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        // using the done promise callback
        .done(function(data) {
            // log data to the console so we can see
            if (!data.id && data.code != 200){
              window.location = '<?php echo base_url(); ?>index.php/Login'
            }

            // here we will handle errors and validation messages
        });
<?php }else{ 
    //if cookie not set redirect to login page
    ?>
    window.location = '<?php echo base_url(); ?>index.php/Login'

    <?php } ?>

// process the form
$('form#fq').submit(function(event) {
    // get the form data
    // there are many ways to get this data using jQuery (you can use the class or id also)
    var formData = {
            'food'          : $('select[name=food]').val(),
            'length'         : $('input[name=length]').val(),
            'weight'           : $('input[name=weight]').val(),
            'color'         : $('input[name=color]').val()
        };
//console.log(formData);
     //if (isset($_COOKIE['food_check_token']))
     
    // process the form
    $.ajax({
        type        : 'GET', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo base_url(); ?>index.php/api/food/v1/poisonous', // the url where we want to POST
        data        : formData, // our data object
        headers     : {'x-header-jwt':jwt_token},
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
        // using the done promise callback
        .done(function(data) {
            // log data to the console so we can see
            console.log(data); 

            $('#msg').html(data.message);
            $('#msg').removeClass("alert-danger alert-success");
            $('#msg').addClass(data.alert);
            $('#msg').show();
            $('#msg').on('click',function(){$(this).hide()});

            // here we will handle errors and validation messages
        });

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

});

</script>
</body>
</html>
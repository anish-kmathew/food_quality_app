<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once('header.php');
?>

  <div class="register-box-body">
    <p class="login-box-msg">Login</p>

    <form action="" method="post">
      
	  <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      
      <div style="display:none" class="alert " role="alert" id='msg'>
    
    </div>

      <div class="row">
        
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    New user? <a href="<?php echo base_url(); ?>index.php/Register" class="text-center">Register</a>
  </div>
  <!-- /.form-box -->
</div><!-- jQuery-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Bootstrap JS-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script>
     
  // magic.js
$(document).ready(function() {

//if already logged in redirect to home page
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo base_url(); ?>index.php/api/login', // the url where we want to POST
        data        : {}, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
        // using the done promise callback
        .done(function(data) {
            // log data to the console so we can see
            if (data.id > 0 || data.code == 200){
              window.location = '<?php echo base_url(); ?>'
            }

            // here we will handle errors and validation messages
        });

// process the form
$('form').submit(function(event) {
    // get the form data
    // there are many ways to get this data using jQuery (you can use the class or id also)
    var formData = {
            'username'          : $('input[name=username]').val(),
            'password'          : $('input[name=password]').val()
        };
    // process the form
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo base_url(); ?>index.php/api/login', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
        // using the done promise callback
        .done(function(data) {
            // log data to the console so we can see
            if (data.id > 0 || data.code == 200){
              window.location = '<?php echo base_url(); ?>'
            }
            $('#msg').html(data.message);
            $('#msg').removeClass("alert-danger alert-success");
            $('#msg').addClass(data.alert);
            $('#msg').show();
            $('#msg').on('click',function(){$(this).hide()});

            // here we will handle errors and validation messages
        });

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

});

</script>
</body>
</html>
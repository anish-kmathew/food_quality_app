<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once('header.php');
?>
  <div class="register-box-body">
    <p class="login-box-msg">Registration</p>

    <form action="" method="post"id ="rg">
      
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	  </div>
	  <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      
      <a href="<?php echo base_url(); ?>index.php/Login" class="text-center">
      <div style="display:none" class="alert " role="alert" id='msg'>
          </div>
      </a>
      <div style="display:none" class="alert " role="alert" id='emsg'>
      </div>
      
      <div class="row">
        
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="<?php echo base_url(); ?>index.php/Login" class="text-center">I already have a membership? Login</a>
  </div>
  <!-- /.form-box -->
</div><!-- jQuery-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Bootstrap JS-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script>
     
  // magic.js
$(document).ready(function() {

// process the form
$('form#rg').submit(function(event) {
    // get the form data
    // there are many ways to get this data using jQuery (you can use the class or id also)
    var formData = {
            'username'          : $('input[name=username]').val(),
            'email'             : $('input[name=email]').val(),
            'password'          : $('input[name=password]').val()
        };
    // process the form
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : '<?php echo base_url(); ?>index.php/api/register', // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
        // using the done promise callback
        .done(function(data) {
            
             // here we will handle errors and validation messages
            if (data.code == 200)
                $('form#rg').find("input[type=text], input[type=password], input[type=email]").val("");

            $('#emsg').html(data.message);
            $('#emsg').removeClass("alert-danger alert-success");
            $('#emsg').addClass(data.alert);
            $('#emsg').show();
            if (data.alert == 'alert-danger' ) 
              $('#emsg').on('click',function(){$(this).hide()});
            else{
              $('#msg').html($('#emsg').html());
              $('#msg').addClass(data.alert);
              $('#emsg').hide();
              $('#msg').show();
            }

           
        });

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
});

});

</script>
</body>
</html>
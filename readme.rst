Project Summary
################

This project contains two part.

Part 1
*******

The web application to check the quality of foods as well as registration, login and payments.

Part 2
*******

The API related to each of the web application needs.

Workflow
########
When we access the application it will be redirected to the login page. 
If you are a new user you can register from there or login by giving the credentials. 
After registration the page will be redirected to payment page where the user has to enter the payment details 
like card number etc. After successful submission you can navigate to login and then to the home page.
Home page is where we can check the quality/poisonous mashrooms.

All the above functionalities are accomplished using APIs. Following are the APIs used


Login API
*********

The Login API is a service that is used to login to the system using HTTP POST.

A Login API request takes the following form:

http://{app_root}/index.php/api/login
**************************************

Users have to post to this URL. The parameters are 

username
===========
password
===========

The return datatype is JSON.
In return data we get response code , message etc which you can see by simply consoling the return data.
On successful login the system will create a JSON WEB TOKEN and stores it in the cookie 
which is used  for the further authentication.
Users have to send this token to the API through HTTP header in order to authenticate the user. 
The token should be passed in the following format.

{x-header-jwt : token}
*********************

If the user is not a member they can register by following the register link. 


Registration API
*****************

The Registration API is a service that is used to register users into the system using HTTP POST.

A Registration API request takes the following form:

http://{app_root}/index.php/api/register
******************************************

Users have to post to this URL. The parameters are 

username
========
email
========
password
========

The return datatype is JSON.

In return data we get response code , message , user_id etc which you can see by simply consoling the return data.
On successful registration the system will prompt the user to do the payment.


Payment API
*************
The Payment API is a service that is used to do the payment transactions using HTTP POST.

A Payment API request takes the following form:

http://{app_root}/index.php/Payment/index/user_id
******************************************************

Here user_id is the id we get from registration response
Users have to post to this URL. The parameters are 

user_id 
========
cc_name 
========
cc_number   
========
cc_exp_mo   
========
cc_exp_yr   
========
cc_cvv   
========
amount  
========

The return datatype is JSON.

In return data we get response code , message etc which you can see by simply consoling the return data.
On successful Payment the system will prompt the user to login and use the application.
After login the user will be redirected to the home page where user can check the quality of mashrooms.
Following is the API.


Poisonous API
****************

The Poisonous API is a service that is used to do the food quality check using HTTP GET.

A Poisonous API request takes the following form:

http://{app_root}/index.php/api/food/v1/poisonous
********************************************************

Here v1 is used to maintain versions of the endpoint.
Users have to post to this URL. The parameters are 

food    
========
length  
========
weight  
========
color 
========

The return datatype is JSON.
In return data we get response code , message etc which you can see by simply consoling the return data. 
The API will return whether the mashroom is poisonous or not.

Note : 
******
In future if we wanted to add more foods and quality end points we can add it in database
and do the corresponding APIs.
